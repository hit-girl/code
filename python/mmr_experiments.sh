#!/bin/bash
test_points=(0,0 0.25,0.25 0.5,0.5 0.75,0.75 1,1)
for i in "${test_points[@]}"
do IFS=","; set -- $i;
    python \
        /app/src/prototype/app.py retrieval \
        --reranking maximal-marginal-relevance --reuse-unranked /data/unranked_dump_final.pkl \
        --lambda-conclusions $1 --lambda-premises $2 \
        mrr_$1_$2_token_factor_1_75 /data/topics.xml /data/results/mmr_symmetric
done
