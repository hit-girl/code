import logging
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
from numpy import ndarray
from tqdm import tqdm


class Indexing:
    """
    TODO
    """
    EMBEDDING_LENGTH = 384
    TOTAL_DUPLICATED_SENTENCES = 5337410

    def __init__(self, input_generator,
                 elastic_host: str = 'elastic',
                 create_indices: bool = True):
        """
        TODO
        """
        self.logger = logging.getLogger(__name__)
        self.input_generator = input_generator
        self.es = Elasticsearch(elastic_host)
        self.create_indices = create_indices
        self.index_schema = self.get_index_schema(self.EMBEDDING_LENGTH)

    def get_index_schema(self, dims: int) -> None:
        """
        TODO
        """
        return {
            "mappings": {
                "properties": {
                    "sentence_text": {
                        "type": "text",
                        "fields": {
                            "length": {
                                "type": "token_count",
                                "analyzer": "standard"
                            }
                        }
                    },
                    "sentence_stance": {
                        "type": "text"
                    },
                    "sentence_vector": {
                        "type": "dense_vector",
                        "dims": dims
                    }
                }
            }
        }

    def recreate_index(self, index_name: str) -> None:
        """
        TODO
        """
        self.logger.info(f'Recreating {index_name} index...')
        if self.es.indices.exists(index=index_name):
            self.es.indices.delete(index=index_name)
        self.es.indices.create(index=index_name, body=self.index_schema, ignore=[400])

    def index_to_es(self) -> None:
        """
        TODO
        """
        if self.create_indices:
            self.recreate_index('premise')
            self.recreate_index('conc')
        self.logger.info('Start indexing...')
        with tqdm(total=self.TOTAL_DUPLICATED_SENTENCES) as progress_bar:
            for ok, action in streaming_bulk(client=self.es, actions=self._generate_actions()):
                progress_bar.update()
        self.logger('Finished indexing!')

    def _generate_actions(self):
        """
        TODO
        """
        for sentence, embedding in self.input_generator:
            yield {"_index": sentence[1].lower(),
                   "_id": '_'.join(sentence[:3]),
                   "_source": {
                "sentence_text": sentence[3],
                "sentence_stance": sentence[4],
                "sentence_vector": embedding
            }
            }
