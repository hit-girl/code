from enum import Enum


class SubCommands(Enum):
    INDEXING = 'indexing'
    RETRIEVAL = 'retrieval'


class RerankingOptions(Enum):
    MAXIMAL_MARGINAL_RELEVANCE = 'maximal-marginal-relevance'
    STRUCTURAL_DISTANCE = 'structural-distance'
    ARGUMENT_RANK = 'argument-rank'
    WORD_MOVER_DISTANCE = 'word-mover-distance'
