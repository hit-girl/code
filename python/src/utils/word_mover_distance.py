from collections import defaultdict

import numpy
import wmd
import spacy
from spacy.language import Language
import libwmdrelax


# ~~~~~~~~~~~~~~~~~ ATTENTION ~~~~~~~~~~~~~~~~~
# The source code of this class is mostly a copy pasta form
# https://github.com/src-d/wmd-relax/blob/master/wmd/__init__.py
# Modified the WDM to a normalized implementation as described on SO
# https://stackoverflow.com/questions/56822056/unnormalized-result-of-word-movers-distance-with-spacy

class SpacySimilarityHook:
    """
    This guy is needed for the integration with `spaCy <https://spacy.io>`_.
    Use it like this:
    ::
        nlp = spacy.load('en_core_web_md')
        nlp.add_pipe(wmd.WMD.SpacySimilarityHook(nlp), last=True)
    It defines :func:`~wmd.WMD.SpacySimilarityHook.compute_similarity()` \
    method which is called by spaCy over pairs of
    `documents <https://spacy.io/docs/api/doc>`_.
    .. automethod:: wmd::WMD.SpacySimilarityHook.__init__
    """

    def __init__(self, nlp, ignore_stops, only_alpha, frequency_processor):
        """
        Initializes a new instance of SpacySimilarityHook class.
        :param nlp: `spaCy language object <https://spacy.io/docs/api/language>`_.
        :param ignore_stops: Indicates whether to ignore the stop words.
        :param only_alpha: Indicates whether only alpha tokens must be used.
        :param frequency_processor: The function which is applied to raw \
                                    token frequencies.
        :type ignore_stops: bool
        :type only_alpha: bool
        :type frequency_processor: callable
        """
        self.nlp = nlp
        self.ignore_stops = ignore_stops
        self.only_alpha = only_alpha
        self.frequency_processor = frequency_processor

    def __call__(self, doc):
        doc.user_hooks["similarity"] = self.compute_similarity
        doc.user_span_hooks["similarity"] = self.compute_similarity
        return doc

    def compute_similarity(self, doc1, doc2):
        """
        Calculates the similarity between two spaCy documents. Extracts the
        nBOW from them and evaluates the WMD.
        :return: The calculated similarity.
        :rtype: float.
        """
        doc1 = self._convert_document(doc1)
        doc2 = self._convert_document(doc2)
        vocabulary = {
            w: i for i, w in enumerate(sorted(set(doc1).union(doc2)))}
        w1 = self._generate_weights(doc1, vocabulary)
        w2 = self._generate_weights(doc2, vocabulary)
        evec = numpy.zeros((len(vocabulary), self.nlp.vocab.vectors_length),
                           dtype=numpy.float32)
        for w, i in vocabulary.items():
            evec[i] = self.nlp.vocab[w].vector
        evec_sqr = (evec * evec).sum(axis=1)
        dists = evec_sqr - 2 * evec.dot(evec.T) + evec_sqr[:, numpy.newaxis]
        dists[dists < 0] = 0
        dists = numpy.sqrt(dists)
        result = None

        # wmd throws error when documents that are empty after preprocessing pipeline
        # This is a dirty fix to rank documents containing junk lower
        try:
            result = libwmdrelax.emd(w1, w2, dists)
        except Exception:
            result = 0
        return result

    def _convert_document(self, doc):
        words = defaultdict(int)
        for t in doc:
            if self.only_alpha and not t.is_alpha:
                continue
            if self.ignore_stops and t.is_stop:
                continue
            words[t.orth] += 1
        return {t: self.frequency_processor(t, v) for t, v in words.items()}

    def _generate_weights(self, doc, vocabulary):
        w = numpy.zeros(len(vocabulary), dtype=numpy.float32)
        for t, v in doc.items():
            w[vocabulary[t]] = v
        w /= w.sum()
        return w


@Language.factory('wmd_component',
                  default_config={
                      "ignore_stops": True,
                      "only_alpha": True,
                      "frequency_processor": lambda t, f: numpy.log(1 + f)}
                  )
def wmd_component(nlp, name, ignore_stops, only_alpha, frequency_processor):
    return SpacySimilarityHook(nlp, ignore_stops, only_alpha, frequency_processor)


nlp = spacy.load('en_core_web_md')
nlp.add_pipe('wmd_component', last=True)


def word_mover_distance(first_sent: str, second_sent: str):
    return nlp(first_sent).similarity(nlp(second_sent))
