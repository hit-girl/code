import os
import logging
from pathlib import Path
from argparse import Namespace
from dotenv import dotenv_values, load_dotenv

from .commands import SubCommands


class Configuration():
    """
    TODO Figure out how to inherit from dict
    """
    keys = {
        SubCommands.INDEXING: ['SENTENCES_PATH',
                               'EMBEDDINGS_PATH',
                               'CREATE_INDEX',
                               'ELASTIC_HOST'],
        SubCommands.RETRIEVAL: ['INPUT_PATH',
                                'OUTPUT_PATH',
                                'RUN_NAME',
                                'TOPIC_NRB',
                                'NRB_CONCLUSIONS_PER_TOPIC',
                                'NRB_PREMISES_PER_CONCLUSION',
                                'MIN_LENGTH_FACTOR',
                                'RERANKING',
                                'REUSE_UNRANKED',
                                'LAMBDA_CONCLUSIONS',
                                'LAMBDA_PREMISES',
                                'MU_CONCLUSIONS',
                                'MU_PREMISES',
                                'WAIT_FOR_ES'],
    }

    def __init__(self, args: Namespace):
        self.logger = logging.getLogger(__name__)
        self.command = SubCommands(args.subparser_name)
        load_dotenv()
        # Read conifg from program arguments, .env file and environment and merge them
        self.config = {**self.read_args(args), **dotenv_values(), **os.environ}

    def read_args(self, args: Namespace) -> dict:
        """
        TODO
        """
        self.logger.info("Reading Config...")
        args_list = None
        # Looks ugly but there is no pattern matching in python :(
        if self.command == SubCommands.INDEXING:
            args_list = [Path(args.sentences_path),
                         Path(args.embeddings_path),
                         args.create_index,
                         args.elastic_host]

        elif self.command == SubCommands.RETRIEVAL:
            args_list = [Path(args.input_path),
                         Path(args.output_path),
                         args.run_name,
                         args.topic_nrb,
                         args.nrb_conclusions_per_topic,
                         args.nrb_premises_per_conclusion,
                         args.min_length_factor,
                         args.reranking,
                         args.reuse_unranked,
                         args.lambda_conclusions,
                         args.lambda_premises,
                         args.mu_conclusions,
                         args.mu_premises,
                         args.wait_for_es
                         ]

        config = dict(zip(self.keys[self.command], args_list))
        self.logger.info(config)

        return config
