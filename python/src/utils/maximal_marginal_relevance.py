import numpy as np
from sentence_transformers import util
from operator import itemgetter


def mmr_generic(container: dict, lambda_=0.5) -> None:
    """
    TODO Reduce cohesion :)
    """
    embeddings = np.array([elem['_source']['sentence_vector']
                           for elem in container])
    cosine_similarities = util.cos_sim(embeddings, embeddings) + 1
    query_element_sim = np.array([elem['_score'] for elem in container])
    surrogate_ids = set(range(len(container)))
    selected = set()
    while selected != surrogate_ids:
        remaining = surrogate_ids - selected
        mmr_scores = [
            (r, mmr_score(lambda_,
                          selected,
                          query_element_sim,
                          cosine_similarities,
                          r))
            for r in remaining]
        next_selected = max(mmr_scores, key=itemgetter(1))
        selected.add(next_selected[0])
        container[next_selected[0]]['mmr'] = next_selected[1]


def mmr_score(lambda_, selected_ids, sim_question, sim_matrix, element_id):
    return lambda_ * float(sim_question[element_id]) - (1 - lambda_) * max(
        [float(sim_matrix[element_id, y]) for y in selected_ids - {element_id}] or [0])
