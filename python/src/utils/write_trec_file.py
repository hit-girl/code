from csv import DictWriter
from pathlib import Path
from typing import OrderedDict


def write_terc_file(result: list, output_path: Path) -> None:
    """
    TODO
    """
    with open(str(output_path), 'w') as csvfile:
        fieldnames = ['qid', 'stance', 'pair', 'rank', 'score', 'tag']
        writer = DictWriter(csvfile, delimiter=' ', fieldnames=fieldnames)
        writer.writeheader()
        for row in result:
            writer.writerow(dict(zip(fieldnames, row)))
            csvfile.flush()
