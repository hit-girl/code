import argparse
from dataclasses import dataclass


from .commands import SubCommands, RerankingOptions


@dataclass
class Text:
    """
    TODO
    """
    description = "Hit-Girl'Argument Retrieval System"
    indexing = 'Sub command to create a semantic index with elastic search.'
    elastic_host = 'The hostname of the server/docker container that runs elastic search.'
    create = 'If flag is present two new indices are created, overriding existing ones.'
    sentences_path = 'The file path to the csv file containing the sentences.'
    embeddings_path = 'The file path to the embeddings of the argument units.'
    retrieval = 'Retrive sentence pairs from the index.'
    topic_nrb = 'Restrict the current indexing and/or reranking to a given topic number.'
    nrb_conclusions = 'The number of conclusions that should be retrieved from the index per topic.'
    nrb_premises = 'The number of premises that should be retrieved from the index per conclusion.'
    run_name = 'The run name that will be included in the last column of the trec file.'
    output_path = 'The file path to the directory containing the output files.'
    input_path = 'The file path to the directory containing the input files.'


def parse_cli_args() -> argparse.Namespace:
    """
    TODO
    """
    parser = argparse.ArgumentParser(description=Text.description)
    subparsers = parser.add_subparsers(required=True, dest='subparser_name')

    # Indexing

    parser_index = subparsers.add_parser(SubCommands.INDEXING.value, help=Text.indexing)
    parser_index.add_argument('--elastic-host', dest='elastic_host', help=Text.elastic_host)
    parser_index.add_argument('--create', dest='create_index',
                              action='store_true',
                              help=Text.create)
    parser_index.add_argument('sentences_path', type=str, help=Text.sentences_path)
    parser_index.add_argument('embeddings_path', type=str, help=Text.embeddings_path)

    # Retrieval

    parser_retrieval = subparsers.add_parser(SubCommands.RETRIEVAL.value, help=Text.retrieval)
    parser_retrieval.add_argument('--topic-nrb',
                                  type=int,
                                  action='append',
                                  default=None,
                                  help=Text.topic_nrb)
    parser_retrieval.add_argument('--nrb-conclusions-per-topic',
                                  type=int,
                                  required=False,
                                  default=100,
                                  help=Text.nrb_conclusions)
    parser_retrieval.add_argument('--nrb-premises-per-conclusion',
                                  type=int,
                                  required=False,
                                  default=25,
                                  help=Text.nrb_premises)
    parser_retrieval.add_argument('--min-length-factor',
                                  type=float,
                                  required=False,
                                  default=1)
    parser_retrieval.add_argument('--reranking',
                                  type=str,
                                  required=False,
                                  choices=[option.value for option in
                                           [RerankingOptions.MAXIMAL_MARGINAL_RELEVANCE,
                                            RerankingOptions.STRUCTURAL_DISTANCE,
                                            RerankingOptions.ARGUMENT_RANK,
                                            RerankingOptions.WORD_MOVER_DISTANCE]
                                           ],
                                  default=None)
    parser_retrieval.add_argument('--reuse-unranked',
                                  type=str,
                                  required=False)
    parser_retrieval.add_argument('--lambda-conclusions',
                                  type=float,
                                  required=False,
                                  default=0.5)
    parser_retrieval.add_argument('--lambda-premises',
                                  type=float,
                                  required=False,
                                  default=0.5)
    parser_retrieval.add_argument('--mu-conclusions',
                                  type=float,
                                  required=False,
                                  default=0.9)
    parser_retrieval.add_argument('--mu-premises',
                                  type=float,
                                  required=False,
                                  default=0.75)
    parser_retrieval.add_argument('--wait-for-es',
                                  action='store_true')

    parser_retrieval.add_argument('run_name', type=str, help=Text.run_name)
    parser_retrieval.add_argument('input_path', type=str, help=Text.input_path)
    parser_retrieval.add_argument('output_path', type=str, help=Text.output_path)

    return parser.parse_args()
