import pickle
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import Tuple
import csv

import numpy as np
import pandas as pd


def read_topics(input_path: Path, topic_nrbs: list = None) -> pd.DataFrame:
    root = ET.parse(str(input_path)).getroot()
    topics = [(int(topic[0].text), topic[1].text) for topic in root]
    topics = pd.DataFrame(topics, columns=['index', 'topic']).set_index('index')
    return topics.loc[topic_nrbs, :] if topic_nrbs else topics


def read_data_to_index(sentences_path: Path,
                       embeddings_path: Path):
    """
    TODO
    """
    with open(str(sentences_path)) as sentences, open(embeddings_path) as embeddings:
        reader1 = csv.reader(sentences)
        # skip header
        next(reader1)
        reader2 = csv.reader(embeddings)
        for row1, row2 in zip(reader1, reader2):
            yield (row1, np.array(row2, dtype=np.float))


def read_embeddings(embeddings_pkl_path: Path) -> np.ndarray:
    return pickle.load(open(embeddings_pkl_path, 'rb'))


def read_sentences(sentences_parquet_path: Path) -> pd.DataFrame:
    return pd.read_parquet(str(sentences_parquet_path))


def read_results(results_path: Path) -> pd.DataFrame:
    return pd.read_csv(str(results_path), delimiter=' ')


def read_unranked(reranked_path: Path, topic_nrbs: list) -> dict:
    unranked = pickle.load(open(str(reranked_path), 'rb'))
    if topic_nrbs:
        unranked = {k: v for k, v in unranked.items() if k in topic_nrbs}
    return unranked
