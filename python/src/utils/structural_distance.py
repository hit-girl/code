import spacy
import textdistance


nlp = spacy.load("en_core_web_md")


def structural_distance(first_sent: str, second_sent: str):

    tags_fist_sent = [token.pos_ for token in nlp(first_sent)]
    tags_second_sent = [token.pos_ for token in nlp(second_sent)]

    return textdistance.jaro.distance(tags_fist_sent, tags_second_sent)
