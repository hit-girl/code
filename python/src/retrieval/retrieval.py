import asyncio
from datetime import timedelta
import re
import os
from typing import List, OrderedDict
from pathlib import Path
import logging

import numpy as np
import pandas as pd
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient
from sentence_transformers import SentenceTransformer
from tqdm import tqdm
from failsafe import Failsafe, RetryPolicy, Delay

class Retrieval:
    """
    TODO
    """
    KIBANA_SPECIAL = '\\ / + - & | ! ( ) { } [ ] ^ " ~ * ? : \\'.split(' ')

    def __init__(self, topics: pd.DataFrame,
                 run_name: str,
                 length_factor: float,
                 host: str = 'elastic',
                 nrb_conclusions_per_topic: int = 3,
                 nrb_premises_per_conclusion: int = 3):
        """
        TODO
        """
        self.logger = logging.getLogger(__name__)
        self.topics = topics
        self.run_name = run_name
        self.length_factor = length_factor
        self.nrb_conclusions_per_topic = nrb_conclusions_per_topic
        self.nrb_premises_per_conclusion = nrb_premises_per_conclusion
        self.es = Elasticsearch(host, timeout=90, max_retries=5)
        self.model = SentenceTransformer(str(Path(os.getenv('SENTENCE_TRANSFORMERS_HOME'),
                'sentence-transformers_all-MiniLM-L6-v2/')))
        self.conclusion_lambda = 0.5
        self.premises_lambda = 0.5

    def retrieve(self) -> dict:
        """
        TODO
        """
        topics_embeddings = self._create_topic_embeddings()
        conclusions_per_topics = self._retrieve_conclusions(list(zip(self.topics['topic']
                                                                .reset_index()
                                                                .to_numpy(),
                                                                topics_embeddings)))
        premises_per_conclusions_per_topics = self._retrieve_premises(conclusions_per_topics)
        return premises_per_conclusions_per_topics

    def _create_topic_embeddings(self) -> np.ndarray:
        """
        TODO
        """
        return self.model.encode(self.topics['topic'].to_numpy())

    def _retrieve_conclusions(self, topics_and_embeddings: List):
        """
        TODO
        """
        conclusions_respons = OrderedDict()
        topics_token_lengths = [len(analyzed_topic['tokens']) for analyzed_topic in self._analyze_bulk([topic[1] for topic, _ in topics_and_embeddings])]
        for topic_embedding, topics_token_length in tqdm(zip(topics_and_embeddings,topics_token_lengths)):
            topic_nrb = topic_embedding[0][0]
            topic_text = topic_embedding[0][1]
            topic_embedding = topic_embedding[1]

            self.logger.info(f"Retrieve conclusion for topic {topic_text}")
            conclusions_respons[topic_nrb] = self.es.search(
                index='conc',
                body=self._get_query_body(
                    self.nrb_conclusions_per_topic,
                    topics_token_length,
                    topic_embedding)
            )
        return conclusions_respons

    def _retrieve_premises(self, conclusion_per_topic: dict) -> dict:
        """
        TODO
        """
        premise_per_conclusion_per_topic = OrderedDict()
        for topic_nrb, conclusions_response in tqdm(conclusion_per_topic.items()):
            self.logger.info(f"Retrieve premises for topic number: {topic_nrb}")
            premise_per_conclusion_per_topic[topic_nrb] = {}
            conclusions = conclusions_response['hits']['hits']

            conclusions_token_lengths = [len(analyzed_conc['tokens']) for analyzed_conc in self._analyze_bulk([conc['_source']['sentence_text'] for conc in conclusions])]
            
            premises_per_conclusion = []


            for conclusion, conclusion_token_length in tqdm(zip(conclusions,conclusions_token_lengths), total=len(conclusions)):
                premises = asyncio.run(self._get_premises(conclusion,conclusion_token_length))
                premises_per_conclusion.append(premises)

            for idx, conclusion_and_premises in enumerate(premises_per_conclusion):
                premise_per_conclusion_per_topic[topic_nrb][idx] = {}
                premise_per_conclusion_per_topic[topic_nrb][idx]['conclusion'] = \
                    conclusion_and_premises[0]
                premise_per_conclusion_per_topic[topic_nrb][idx]['premises'] = []
                for premise in conclusion_and_premises[1]['hits']['hits']:
                    premise_per_conclusion_per_topic[topic_nrb][idx]['premises'].append(premise)

        return premise_per_conclusion_per_topic

    async def _get_premises(self, conclusion: dict, conclusion_token_length) -> dict:
        delay = Delay(timedelta(seconds=5))
        retry_policy = RetryPolicy(allowed_retries=3, backoff=delay)

        body = self._get_query_body(self.nrb_premises_per_conclusion,
                                    conclusion_token_length,
                                    conclusion['_source']['sentence_vector'])
        body['query']['script_score']['query']['bool']['must'] = {
            'match': {
                'sentence_stance': conclusion['_source']['sentence_stance']
            }
        }

        return Failsafe(retry_policy=retry_policy).run(self.es.search(index='premise', body=body, timeout="120s"))

    def _get_query_body(self, size: int, query_token_length: int, sentence_embedding: np.ndarray) -> dict:
        """
        TODO
        """
        return {
            "size": size,
            "query": {
                "script_score": {
                    "query": {
                        "bool": {
                            "filter": {
                                "range": {
                                    "sentence_text.length": {
                                        "gte": query_token_length * self.length_factor
                                    }
                                }
                            }
                        }
                    },
                    "script": {
                        "source": "cosineSimilarity(params.queryVector,'sentence_vector') + 1.0",
                        "params": {
                            "queryVector": sentence_embedding
                        }
                    }
                }
            }
        }

    def _analyze_bulk(self, sentences : List[str]):
        return [IndicesClient(self.es).analyze(body=dict(text=sentence)) for sentence in sentences]

    def escape(self, query):
        """
        TODO
        """
        # Fucking magic from Stack Overflow
        return re.sub('([{}])'.format('\\'.join(Retrieval.KIBANA_SPECIAL)),
                      r'\\\1',
                      query)
