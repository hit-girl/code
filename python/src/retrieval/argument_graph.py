import networkx as nx
import logging
from utils import word_mover_distance


class ArgumentGraph:

    def __init__(self, premises_per_conclusions_per_topics: dict, threshold: float = 0.20):
        self.logger = logging.getLogger(__name__)
        self.premises_per_conclusions_per_topics = premises_per_conclusions_per_topics
        self.threshold = threshold

    def create(self) -> dict:
        logging.info('Starting to create the argument graphs...')
        graphs = {}
        for topic_nrb, premises_per_conclusions in self.premises_per_conclusions_per_topics.items():
            graph = nx.DiGraph()
            sim_matrices = {}
            for conc_nrb, premises_per_conclusion in premises_per_conclusions.items():
                conclusion = premises_per_conclusion['conclusion']
                sim_matrices[conc_nrb] = [
                    1 /
                    (1 +
                     word_mover_distance(
                         conclusion['_source']['sentence_text'],
                         p['_source']['sentence_text']))
                    for p in premises_per_conclusion['premises']]

            for premises_per_conclusion in premises_per_conclusions.values():
                conclusions_argument_id = conclusion["_id"].split('_')[0]
                premises = premises_per_conclusion['premises']
                if conclusions_argument_id not in graph:
                    graph.add_node(conclusions_argument_id)
                for prem_idx, premise in enumerate(premises):
                    premise_argument_id = premise["_id"].split('_')[0]
                    if premise_argument_id not in graph:
                        graph.add_node(premise_argument_id)
                    # else: TODO add a list of sentence_text to the nodes
                        # graph.nodes[premise_argument_id]['premises']
                    for conc_idx in range(len(premises_per_conclusions)):
                        sim = sim_matrices[conc_idx][prem_idx]
                        if sim >= self.threshold:
                            conc_arg_id = \
                                premises_per_conclusions[conc_idx]['conclusion']['_id'].split(
                                    '_')[0]
                            graph.add_edge(conc_arg_id, premise_argument_id, weight=sim)
            graphs[topic_nrb] = graph
            logging.info(f'Created argument graph for topic nrb {topic_nrb}')
        logging.info('Finished creating the argument graphs!')
        return graphs
