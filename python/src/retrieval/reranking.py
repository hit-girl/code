from collections import OrderedDict
from itertools import chain
import logging
from typing import Callable

from utils import structural_distance, mmr_generic, word_mover_distance
from .argument_graph import ArgumentGraph
from tqdm import tqdm

from abc import abstractmethod


class Reranking:

    def __init__(self, premises_per_conclusions_per_topics: dict,
                 reranking_key: str,
                 run_name: str
                 ):
        self.logger = logging.getLogger(__name__)
        self.premises_per_conclusions_per_topics = premises_per_conclusions_per_topics
        self.reranking_key = reranking_key
        self.run_name = run_name

    @abstractmethod
    def rerank(self):
        pass

    def _rerank(self,
                calculate_scores: Callable,
                reranking_strategy_conclusions: Callable,
                reranking_strategy_premises: Callable,
                min_max_normalizaion=False
                ):
        self.logger.info('Starting to calculate reranking scores...')
        calculate_scores()
        self.logger.info('Finished to calculate reranking scores!')

        if min_max_normalizaion:
            self._min_max_normalizaion()

        self.logger.info('Starting reranking...')
        self._sort(reranking_strategy_conclusions, reranking_strategy_premises)
        self.logger.info('Finished reranking!')

    def _sort(
        self,
        reranking_strategy_conclusions: Callable,
        reranking_strategy_premises: Callable
    ) -> None:
        reranked_premises_per_conclusions_per_topics = OrderedDict()
        # Sort directory entries according to the reranking strategies
        for topic_nrb, premises_pre_conclusions in self.premises_per_conclusions_per_topics.items():
            for premises_per_conclusion in premises_pre_conclusions.values():
                premises_per_conclusion['conclusion']['final_reranking_score'] = \
                    reranking_strategy_conclusions(
                    premises_per_conclusion)

                for premise in premises_per_conclusion['premises']:
                    premise['final_reranking_score'] = reranking_strategy_premises(premise)

                premises_per_conclusion['premises'] = sorted(
                    premises_per_conclusion['premises'],
                    key=lambda x: x['final_reranking_score'],
                    reverse=True)

            reranked_premises_per_conclusions_per_topics[topic_nrb] = OrderedDict(
                enumerate(sorted(premises_pre_conclusions.values(),
                                 key=lambda x: x['conclusion']['final_reranking_score'],
                                 reverse=True)
                          )
            )

        self.premises_per_conclusions_per_topics = reranked_premises_per_conclusions_per_topics

    def _generic_score_calculation(self, scoring_function: Callable) -> None:
        def specific_score_calcualation() -> None:
            for topic_nrb, premises_per_conclusions in tqdm(
                    self.premises_per_conclusions_per_topics.items()):
                for premises_per_conclusion in tqdm(premises_per_conclusions.values()):
                    premises_per_conclusion['conclusion'][self.reranking_key] = scoring_function(
                        premises_per_conclusion['conclusion']['_source']['sentence_text'],
                        self.topics.loc[topic_nrb]['topic'])
                    for premise in premises_per_conclusion['premises']:
                        premise[self.reranking_key] = scoring_function(
                            premise['_source']['sentence_text'],
                            premises_per_conclusion['conclusion']['_source']['sentence_text'])
        return specific_score_calcualation

    def _min_max_normalizaion(self):
        def _min_max_norm(max, min, value):
            return (value - min) / (max - min)

        for topic_nrb, premises_per_conclusions in \
                self.premises_per_conclusions_per_topics.items():
            conclusion_scores = [c['conclusion'][self.reranking_key] for c in
                                 premises_per_conclusions.values()]
            min_conclusion_score = min(conclusion_scores)
            max_conclusion_score = max(conclusion_scores)

            for premises_per_conclusion in premises_per_conclusions.values():
                premises_per_conclusion['conclusion'][
                    self.reranking_key] = _min_max_norm(
                    max_conclusion_score, min_conclusion_score,
                    premises_per_conclusion['conclusion'][self.reranking_key])

                premise_scores = [p[self.reranking_key] for p in
                                  premises_per_conclusion['premises']]
                min_premise_score = min(premise_scores)
                max_premise_score = max(premise_scores)

                for p in premises_per_conclusion['premises']:
                    p[self.reranking_key] = _min_max_norm(
                        max_premise_score, min_premise_score, p[self.reranking_key])

    def generate_trec_rows(self):
        rows_per_topic = OrderedDict()
        for topic_nrb, premises_pre_conclusions in self.premises_per_conclusions_per_topics.items():
            trec_style_rows = []
            encountered_premise_ids = set()
            for premises_per_conclusion in premises_pre_conclusions.values():
                conclusion = premises_per_conclusion['conclusion']
                for premise in premises_per_conclusion['premises']:
                    stance_conclusion = conclusion['_source']['sentence_stance']
                    stance_premise = premise['_source']['sentence_stance']
                    if stance_conclusion == stance_premise \
                            and premise['_id'] not in encountered_premise_ids:
                        trec_style_rows.append(
                            (topic_nrb, conclusion['_source']['sentence_stance'],
                             f'{"__".join(premise["_id"].split("_"))},' +
                             f'{"__".join(conclusion["_id"].split("_"))}',
                             len(trec_style_rows),
                             f'{conclusion["final_reranking_score"]:.2f}', self.run_name))
                        encountered_premise_ids.add(premise['_id'])
                        break
                rows_per_topic[topic_nrb] = trec_style_rows

        return chain.from_iterable(rows_per_topic.values())


class MaximalMarginalRelevanceReranking(Reranking):

    def __init__(self, premises_per_conclusions_per_topics: dict,
                 run_name: str,
                 lambdac: float = 0.5,
                 lambdap: float = 0.5):
        super().__init__(premises_per_conclusions_per_topics, 'mmr', run_name)
        self.lambdac = lambdac
        self.lambdap = lambdap

    def rerank(self) -> None:

        def reranking_strategy_conclusions(x):
            return x['conclusion'][self.reranking_key]

        def reranking_strategy_premises(x):
            return x[self.reranking_key]

        super()._rerank(
            self._mmr_conclusions_and_premises,
            reranking_strategy_conclusions,
            reranking_strategy_premises)

    def _mmr_conclusions_and_premises(self):
        """
        TODO
        """
        for _, premises_per_conclusions in tqdm(self.premises_per_conclusions_per_topics.items()):
            conclusions = [element['conclusion'] for element in premises_per_conclusions.values()]
            mmr_generic(conclusions, self.lambdac)
            for premises_per_conclusion in tqdm(premises_per_conclusions.values()):
                mmr_generic(premises_per_conclusion['premises'], self.lambdap)


class StructuralDistanceReranking(Reranking):
    def __init__(self, premises_per_conclusions_per_topics: dict,
                 run_name: str,
                 topics,
                 mup,
                 muc):
        super().__init__(premises_per_conclusions_per_topics, 'sd', run_name)
        self.topics = topics
        self.mup = mup
        self.muc = muc

    def rerank(self) -> None:
        def reranking_strategy_conclusions(x):
            return x['conclusion'][
                self.reranking_key] * (
                1 - self.muc) + x['conclusion']['_score'] / 2 * self.muc

        def reranking_strategy_premises(x):
            return x[self.reranking_key] * (1 - self.mup) + x['_score'] / 2 * self.mup

        super()._rerank(
            super()._generic_score_calculation(structural_distance),
            reranking_strategy_conclusions,
            reranking_strategy_premises)


class WordMoverDistanceReranking(Reranking):
    def __init__(self, premises_per_conclusions_per_topics: dict,
                 run_name: str,
                 topics,
                 mup,
                 muc):
        super().__init__(premises_per_conclusions_per_topics, 'wmd', run_name)
        self.topics = topics
        self.mup = mup
        self.muc = muc

    def rerank(self) -> None:
        def reranking_strategy_conclusions(x):
            return x['conclusion'][
                self.reranking_key] * (
                1 - self.muc) + x['conclusion']['_score'] / 2 * self.muc

        def reranking_strategy_premises(x):
            return x[self.reranking_key] * (1 - self.mup) + x['_score'] / 2 * self.mup

        super()._rerank(
            self._generic_score_calculation(word_mover_distance),
            reranking_strategy_conclusions,
            reranking_strategy_premises, True)


class ArgumentRankReranking(Reranking):

    def __init__(self, premises_per_conclusions_per_topics: dict,
                 run_name: str,
                 topics):
        super().__init__(premises_per_conclusions_per_topics, 'arg_rank', run_name)
        self.graphs = ArgumentGraph(premises_per_conclusions_per_topics).create()

    def rerank(self) -> None:
        def reranking_strategy_conclusions(x):
            return x['conclusion'][self.reranking_key]

        def reranking_strategy_premises(x):
            return x[self.reranking_key]

        super()._rerank(
            self._arg_rank,
            reranking_strategy_conclusions,
            reranking_strategy_premises)

    def _arg_rank(self):
        for topic_nrb, g in self.graphs.items():
            # TODO
            pass


class NoReranking(Reranking):
    def __init__(self, premises_per_conclusions_per_topics: dict,
                 run_name: str):
        super().__init__(premises_per_conclusions_per_topics, '_score', run_name)

    def rerank(self):
        pass
