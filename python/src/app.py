from pathlib import Path
from indexing import Indexing
from retrieval import (Retrieval, MaximalMarginalRelevanceReranking, StructuralDistanceReranking,
                       ArgumentRankReranking, WordMoverDistanceReranking, NoReranking)
from utils import (Configuration, SubCommands, RerankingOptions, parse_cli_args,
                   read_data_to_index, read_unranked, read_topics, write_terc_file)
import logging
import time

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s - %(message)s', level='INFO')
logging.Logger.manager.loggerDict["elastic_transport.transport"].disabled = True


class App:

    def __init__(self, configuration: Configuration):
        self.logger = logging.getLogger(__name__)
        self.command = configuration.command
        self.config = configuration.config

    def main(self) -> None:
        """
        TODO
        """

        self.logger.info("Welcome to the Hit-Girl's retrieval system")
        if self.command == SubCommands.INDEXING:
            self._indexing()
        elif self.command == SubCommands.RETRIEVAL:
            self._retrieval()

    def _indexing(self) -> None:
        self.logger.info('Reading sentences and embeddings form disk.')
        input_generator = read_data_to_index(self.config['SENTENCES_PATH'],
                                             self.config['EMBEDDINGS_PATH'])
        Indexing(input_generator,
                 self.config['ELASTIC_HOST'],
                 self.config['CREATE_INDEX']
                 ).index_to_es()

    def _retrieval(self) -> None:

        topics = read_topics(
            Path(self.config['INPUT_PATH'],
                 'topics.xml'),
            self.config['TOPIC_NRB'])
        self.logger.info('Read topics!')
        retrieved_results = None
        if self.config['REUSE_UNRANKED']:
            self.logger.info('Reading unranked...')
            retrieved_results = read_unranked(self.config['REUSE_UNRANKED'],
                                              self.config['TOPIC_NRB'])
            self.logger.info('Read unranked!')
        else:
            if self.config['WAIT_FOR_ES']:
                self.logger.info('Waiting 45 minutes for elasticsearch ...')
                time.sleep(4500)
                self.logger.info('Waited for elasticsearch!')

            retrieved_results = Retrieval(topics,
                                          self.config['RUN_NAME'],
                                          self.config['MIN_LENGTH_FACTOR'],
                                          self.config['ELASTIC_HOST'],
                                          self.config['NRB_CONCLUSIONS_PER_TOPIC'],
                                          self.config['NRB_PREMISES_PER_CONCLUSION']
                                          ).retrieve()

        # Pattern matching is only available since Python 3.10 :(
        reranker = None
        if self.config['RERANKING'] == RerankingOptions.MAXIMAL_MARGINAL_RELEVANCE.value:
            reranker = MaximalMarginalRelevanceReranking(
                retrieved_results,
                self.config['RUN_NAME'],
                self.config['LAMBDA_CONCLUSIONS'],
                self.config['LAMBDA_PREMISES']
            )
        elif self.config['RERANKING'] == RerankingOptions.STRUCTURAL_DISTANCE.value:
            reranker = StructuralDistanceReranking(
                retrieved_results, self.config['RUN_NAME'],
                topics, self.config['MU_PREMISES'],
                self.config['MU_CONCLUSIONS'])
        elif self.config['RERANKING'] == RerankingOptions.ARGUMENT_RANK.value:
            reranker = ArgumentRankReranking(retrieved_results, self.config['RUN_NAME'], topics)
        elif self.config['RERANKING'] == RerankingOptions.WORD_MOVER_DISTANCE.value:
            reranker = WordMoverDistanceReranking(
                retrieved_results, self.config['RUN_NAME'],
                topics, self.config['MU_PREMISES'],
                self.config['MU_CONCLUSIONS'])
        else:
            reranker = NoReranking(retrieved_results, self.config['RUN_NAME'])

        reranker.rerank()
        self.logger.info("Writing run.txt ...")
        write_terc_file(reranker.generate_trec_rows(),
                        Path(self.config['OUTPUT_PATH'], 'run.txt')
                        )
        self.logger.info("Wrote run.txt!")
        self.logger.info("Finished retrieval!")


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logger.info("Welcome to the Hit-Girl's retrieval system")
    App(Configuration(parse_cli_args())).main()
