#%%
import pickle
import pandas as pd
import faiss

#%%
sentences = pd.read_parquet('../data/sentences.parquet.snappy').drop_duplicates('sent_text')
embeddings = pickle.load(open('../data/embeddings.pkl', 'rb'))[sentences.index]
sentences.reset_index(drop=True, inplace=True)
# %%
k = len(sentences[sentences['sent_type'] == 'CONC'])
kmeans = faiss.Kmeans(d=embeddings.shape[1], k=k, niter=20)
kmeans.train(embeddings[sentences[sentences['sent_type'] == 'PREMISE'].index])
