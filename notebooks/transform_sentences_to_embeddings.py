""" 
transform_sentences_to_embeddings.py is intended to run in an environment that 
has at least a Nvidia K-80 accelerator available.
It reads the parquet file generated from the script
'transform_args_processed_to_parquet.py' and generates an embedding for each
sentence.
"""
import pickle

import numpy as np
import pandas as pd
import torch
from sentence_transformers import SentenceTransformer
from tqdm import tqdm

DATA_DIR = '../data'
INPUT_FILE_PATH = f'{DATA_DIR}/sentences.parquet.snappy'
OUTPUT_FILE_PATH = f'{DATA_DIR}/embeddings.pkl'


def read_sentences(file_path: str) -> pd.DataFrame:
    return pd.read_parquet(file_path)


def create_embeddings(df: pd.DataFrame) -> np.ndarray:
    """
    TODO
    """

    model = SentenceTransformer('all-MiniLM-L6-v2', 'cuda')
    embeddings = list()
    # Split the sentences into batches of 1000, otherwise the SentenceTransfromer
    # will silently use the CPU instead of the GPU, because V-RAM would be not
    # sufficient
    for split in tqdm(np.array_split(df['sent_text'].to_numpy(), len(df) // 1000)):
        torch.cuda.empty_cache()
        embeddings.append(model.encode(split, batch_size=32))

    return np.vstack(embeddings)


def pickle_embeddings(embeddings: np.ndarray, file_path: str) -> None:
    """
    TODO
    """
    pickle.dump(embeddings, open(file_path, "wb"))


def print_stats(embeddings: np.ndarray) -> None:
    """
    TODO
    """
    print('Total amount of embeddings: ', len(embeddings))
    print('Length of an embedding: ', len(embeddings[0]))


def main(input_path: str, output_path: str) -> None:
    """
    TODO
    """
    df = read_sentences(input_path)
    embeddings = create_embeddings(df)
    pickle_embeddings(embeddings, output_path)
    print_stats(embeddings)


if __name__ == "__main__":
    main(INPUT_FILE_PATH, OUTPUT_FILE_PATH)
