"""
transform_args_processed_to_parquet.py transforms the messy args_processed.csv 
into a sane columnar fromat using the parquet file protocol
Script Arguments:
  None
Side effects:
  Creates a new parquet file and write it to the global variable OUTPUT_FILE_PATH
  Schema of the parquet file: 
      | sent_type | sent_pos | sent_id | sent_text | stance |
"""
import ast
import csv
import sys
from concurrent.futures import ProcessPoolExecutor

import pandas as pd
from tqdm import tqdm

DATA_DIR = '../data'
INPUT_FILE_PATH = f'{DATA_DIR}/args_processed.csv'
OUTPUT_FILE_PATH = f'{DATA_DIR}/sentences.parquet.snappy'
ARGS_TOTAL = 365409


def extract_argument_unit(row: str):
    """
    TODO
    """
    stance = ast.literal_eval(row['premises'])[0]['stance']
    out = dict(arg_id=list(), sent_type=list(), sent_pos=list(), sent_text=list(), stance=list())

    for sent in ast.literal_eval(row['sentences']):
        splitted = sent['sent_id'].split('__')
        out['sent_type'].append(splitted[1])
        out['sent_pos'].append(splitted[2])
        out['arg_id'].append(row['id'])
        out['sent_text'].append(sent['sent_text'])
        out['stance'].append(stance)
    return pd.DataFrame(out)


def transfrom_data_parallel(reader: csv.DictReader) -> pd.DataFrame:
    """
    TODO
    """
    data_frames = list()
    with ProcessPoolExecutor() as executor:
        with tqdm(total=ARGS_TOTAL) as progress:
            futures = list()
            for row in reader:
                future = executor.submit(extract_argument_unit, row)
                future.add_done_callback(lambda p: progress.update())
                futures.append(future)
            for future in futures:
                data_frames.append(future.result())
    return pd.concat(data_frames)


def write_data_frame(df: pd.DataFrame, output_path: str) -> None:
    """
    TODO
    """
    df = df.reset_index(drop=True)
    df.to_parquet(OUTPUT_FILE_PATH, compression='snappy')


def main(input_path: str, output_path: str) -> None:
    """
    TODO
    """
    with open(input_path, 'r') as file:
        reader = csv.DictReader(file)
        csv.field_size_limit(sys.maxsize)
        df = transfrom_data_parallel(reader)
        write_data_frame(df, output_path)


if __name__ == "__main__":
    main(INPUT_FILE_PATH, OUTPUT_FILE_PATH)
