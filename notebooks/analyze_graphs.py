# %%
import pandas as pd
import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import torch

graphs_wdm = pickle.load(open('../data/wdm_arg_rank.pkl', 'rb'))
graphs_cosine = pickle.load(open('../data/cosine_arg_rank.pkl', 'rb'))

#%%
def all_sim(graphs):
    return [attrs['weight'] for g in graphs.values() for a, b, attrs in g.edges(data=True)]

all_sim_wdm = pd.Series(all_sim(graphs_wdm))
all_sim_cosine = pd.Series(torch.stack(all_sim(graphs_cosine)))

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.set_xlabel("Word Mover's Similarity")
ax2.set_xlabel('Cosine Similarity')
ax1.boxplot(all_sim_wdm)
ax2.boxplot(all_sim_cosine)

plt.tight_layout()
plt.savefig('./plots/cosine_wdm_sim_scores.pdf')

#%%
graphs = graphs_wdm
graph_type = 'wdm'
#%%
for g in graphs.values():
    to_remove = [(a,b) for a, b, attrs in g.edges(data=True) if attrs["weight"] < 0.35]
    g.remove_edges_from(to_remove)
#%%
argument_nodes = pd.Series(
    [len(g.nodes) for g in graphs.values()],
    name='Argument Nodes per Topic', index=graphs.keys())
argument_edges = pd.Series(
    [len(g.edges) for g in graphs.values()],
    name='Argument Edges per Topic', index=graphs.keys())


# %%
fig, (ax1, ax2) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [4, 1]}, figsize=(10, 5))
# fig.suptitle('Argument Edges')
ax1.bar(argument_edges.index, argument_edges)
ax1.set_xlabel('Topic number')
ax1.set_ylabel('# Edges')
ax2.boxplot(argument_edges)
ax2.yaxis.set_ticklabels([])
ax2.xaxis.set_ticklabels([])
plt.savefig(f'./plots/{graph_type}_argument_edges.pdf')

# %%
fig, (ax1, ax2) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [4, 1]}, figsize=(10, 5))
fig.suptitle('Argument Nodes')
ax1.bar(argument_nodes.index, argument_nodes)
ax2.boxplot(argument_nodes)
ax1.set_xlabel('Topic number')
ax1.set_ylabel('# Nodes')
ax2.yaxis.set_ticklabels([])
ax2.xaxis.set_ticklabels([])
plt.savefig(f'./plots/{graph_type}_argument_nodes.pdf')


# %%

fig, axis = plt.subplots(1, 5, figsize=(13, 5))
# fig.suptitle('Argument Graphs per Topic')
half_graphs = list(graphs.items())[:5]
for ax, elem in zip(axis.flatten(), half_graphs):
    topic_nrb = elem[0]
    ax.set_title(topic_nrb)
    g = elem[1].to_undirected()
    Gcc = g.subgraph(sorted(nx.connected_components(g), key=len, reverse=True)[0])
    pos = nx.spring_layout(Gcc, seed=10396953)
    nx.draw_networkx_nodes(Gcc, pos, ax=ax, node_size=20)
    nx.draw_networkx_edges(Gcc, pos, ax=ax, alpha=0.4)
plt.savefig(f'./plots/{graph_type}_argument_graphs.pdf')

# %%

fig, axis = plt.subplots(5, 10, figsize=(20, 15))
for ax, elem in zip(axis.flatten(), graphs.items()):
    topic_nrb = elem[0]
    ax.set_title(topic_nrb)
    g = elem[1]
    f = nx.Graph()
    fedges = filter(lambda x: g.degree()[x[0]] > 1 and g.degree()[x[1]] > 1, g.edges())
    f.add_edges_from(fedges)
    ax.hist(nx.pagerank(f).values())


# %%
degree_list = [g.degree() for g in graphs.values()]
degree_list = [item[1] for sublist in degree_list for item in sublist]
fig, ax = plt.subplots()
# ax.set_title('Degrees Histogramm of Argument Graphs (all Topics)')
ax.set_yscale('log')
ax.set_xlabel("Degree")
ax.set_ylabel("# of Nodes")
ax.hist(degree_list)

plt.savefig(f'./plots/{graph_type}_degree_histogramm.pdf')


# %%
