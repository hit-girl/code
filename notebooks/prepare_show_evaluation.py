# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
# %%
# READ ME
# This notebook helps zu evaluate a .trec file by hand,
#  with quality dimensions 'quality' and 'relevance'
# Functions are defined at the end of this file
# To create an excel evaluation file, call create_evaluation_excel
# after filling in the columns in your excel, you can execute the
# 'Read and Plot done Evaluations' Code Block using your excel file path

# %%
# FUNCTIONS DEFENITIONS


def lookup_text(idd: str, lookup_dict):
    try:
        return(lookup_dict[idd])
    except KeyError:
        return ""


def create_evaluation_excel(trec_file: str, output_file: str, sentences_file: str):

    # create lookup dict
    df_sentences = pd.read_parquet(sentences_file, engine='pyarrow')
    dict_temp = df_sentences.to_dict(orient='records')
    dict_lookup = {f'{x["arg_id"]}_{x["sent_type"]}_{x["sent_pos"]}': x['sent_text']
                   for x in dict_temp}

    # open output_trec_file
    f = open(trec_file)
    data = []
    for val in f:
        data.append(val.replace(",", " ").split())

    header = ["topic_id", "Stance", "conclusion_sent_id",
              "premise_sent_id", "rank", "sim", "source"]
    df_trec = pd.DataFrame(data[1:], columns=header)

    df_trec['premise_sent_text'] = df_trec.apply(lambda row:
                                                 lookup_text(row.premise_sent_id,
                                                             dict_lookup), axis=1)
    df_trec['conclusion_sent_text'] = df_trec.apply(lambda row:
                                                    lookup_text(row.conclusion_sent_id,
                                                                dict_lookup), axis=1)

    df_out = df_trec[['topic_id', 'rank', 'premise_sent_text', 'conclusion_sent_text']]
    df_out.to_excel(output_file)
    print(f'evaluation excel succesfully saved at {output_file}')


def calc_dcg(list_args_relevance: list):
    """
    Calculates dcg given a list of relevances.

    Parameters

    list_args_relevance: list of relevances, position in list represents rank
    """
    cum_sum = 0
    i = 1
    for rel in list_args_relevance:
        cum_sum += ((math.pow(2, rel) - 1) / math.log((1 + i), 2))
        i += 1

    return cum_sum


def calc_avg_over_topics(ndcg_vals=dict):
    assert(len(ndcg_vals) > 0)
    return(sum(ndcg_vals.values()) / len(ndcg_vals))


def calc_ndcg_values(df: pd.DataFrame, col_name_relevance: str, col_name_topic: str, k: int):
    """
    Calculate ndcg values for a dataframe with sentence pairs on different topics.
    Assumes that the order of the sentence pairs within the dataframe
    represents the ranking of the sentence pairs.
    """
    topics = df[col_name_topic].unique()
    ndcg_values = {}
    for topic in topics:
        df_topic = df[df[col_name_topic] == topic]
        dcg_val = calc_dcg(df_topic[col_name_relevance][0:k])
        # calc optimal dcg
        dcg_opt = calc_dcg(sorted(df_topic[col_name_relevance], reverse=True)[0:k])
        if(dcg_opt > 0):
            ndcg = dcg_val / (dcg_opt + 1)
        else:
            ndcg = 0
        ndcg_values[f'{topic}'] = ndcg

    return ndcg_values


# %%
# Create evaluation file
trec_file_path = ''
excel_output = ''
sentences_file = ''
create_evaluation_excel(trec_file=trec_file_path, output_file=excel_output,
                        sentences_file=sentences_file)


# %%
# Read and plot excel file with evaluations
excel_filepath = ''
df_eval = pd.read_excel(excel_filepath)
assert("quality" in df_eval.columns)
assert("relevance" in df_eval.columns)
df_eval.quality.fillna(0, inplace=True)
df_eval.relevance.fillna(0, inplace=True)

# %%
# Print basic statistics
print(f'statistics over measure quality:  \n{df_eval.quality.describe()}')
print('____________')
print(f'statistics over measure relevance: \n{df_eval.relevance.describe()}')


# %%
# Plot quality in box plots
D = np.array(object=[df_eval.quality, df_eval.relevance]).T
fig, ax = plt.subplots()
VP = ax.boxplot(D, positions=[2, 6], widths=1.5, patch_artist=True,
                showmeans=False, showfliers=False,
                medianprops={"color": "black", "linewidth": 1.5},
                boxprops={"facecolor": "lightblue", "edgecolor": "black",
                          "linewidth": 1.5},
                whiskerprops={"color": "black", "linewidth": 1.5},
                capprops={"color": "black", "linewidth": 1.5})

ax.set(xlim=(0, 8), ylim=(-0.5, 5))
ax.set_xlabel("quality and relevance values boxplot")
ax.tick_params(labelbottom=False)

plt.show()
