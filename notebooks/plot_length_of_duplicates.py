# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient

es = Elasticsearch('localhost:9200')
df = pd.read_parquet('../data/sentences.parquet.snappy')
# embeddings = pickle.load(open('../data/embeddings.pkl', 'rb'))

# %%
conclusions = df[df['sent_type'] == 'CONC']
premises = df[df['sent_type'] == 'PREMISE']

duplicates_indices_conclusions = conclusions['sent_text'].duplicated(keep=False)
duplicates_indices_premises = premises['sent_text'].duplicated(keep=False)


# Distribution of string length
# %%
a = conclusions[duplicates_indices_conclusions]['sent_text'].drop_duplicates()
b = premises[duplicates_indices_premises]['sent_text'].drop_duplicates()
# %%
a = [len(IndicesClient(es).analyze(body=dict(text=chunk))) for chunk in np.array_split(a.to_numpy(),
                                                                                       5_000)]
b = [len(IndicesClient(es).analyze(body=dict(text=chunk))) for chunk in np.array_split(b.to_numpy(),
                                                                                       5_000)]

# %%
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))
fig.suptitle('Token length of duplicates')
ax1.boxplot(a)
ax1.set_xlabel("Conclusions")
ax1.xaxis.set_ticklabels([])
ax2.boxplot(b)
ax2.set_xlabel("Premises")
ax2.xaxis.set_ticklabels([])
plt.savefig('plots/boxplots_amount_of_tokens_of_premise_and_conclusion_duplicates.svg')

# %%
