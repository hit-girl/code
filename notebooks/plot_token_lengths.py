# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient

# %%
es = Elasticsearch('localhost:9200')
df = pd.read_parquet('../data/sentences.parquet.snappy').drop_duplicates('sent_text')
df.reset_index(drop=True, inplace=True)
df = df[df['sent_text'].str.len().between(42, 420)]

# %%


def plot_token_length(sentences, title_text_arg_type):
    token_lengths = [len(IndicesClient(es).analyze(body=dict(text=chunk))['tokens']) for chunk in
                     np.array_split(sentences.to_numpy(), 50_000)]
    fig, ax = plt.subplots()
    # Plot.
    plt.rc('figure', figsize=(8, 6))
    plt.rc('font', size=14)
    plt.rc('lines', linewidth=2)
    # plt.rc('axes', color_cycle=('#377eb8','#e41a1c','#4daf4a',
    #                             '#984ea3','#ff7f00','#ffff33'))
    # Histogram.
    ax.hist(token_lengths, bins=20)
    # Average length.
    avg_len = sum(token_lengths) / float(len(token_lengths))
    plt.axvline(avg_len, color='#e41a1c')
    trans = ax.get_xaxis_transform()
    plt.title(f'Histogram of amount of tokens in {title_text_arg_type}.')
    plt.xlabel('# Tokens')
    plt.text(avg_len+avg_len*0.2, 0.5, 'mean = %.2f' % avg_len, transform=trans)
    plt.show()
    plt.savefig(f'plots/histogramm_amount_of_tokens_{title_text_arg_type}.svg')


# %%
plot_token_length(df[df['sent_type'] == 'CONC']['sent_text'], 'Conclusions')
# %%
plot_token_length(df[df['sent_type'] == 'PREMISE']['sent_text'], 'Premises')
