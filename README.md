# Code

This is the argument retrieval system of team hitgirl. Source code is located under `./python/src`.

![system_architecure](./system_architecture.png)

## CLI

The CLI interface of our system has two sub commands: `indexing` and `retrieval`.
The entery to the runtime can be found in `./docker/docker-compose.dev.yaml`.

### Indexing

```bash
usage: app.py indexing [-h] [--elastic-host ELASTIC_HOST] [--create]
                       sentences_path embeddings_path

positional arguments:
  sentences_path        The file path to the csv file containing the
                        sentences.
  embeddings_path       The file path to the embeddings of the argument units.

optional arguments:
  -h, --help            show this help message and exit
  --elastic-host ELASTIC_HOST
                        The hostname of the server/docker container that runs
                        elastic search.
  --create              If flag is present two new indices are created,
                        overriding existing ones.
```

### Retrieval

```bash
usage: app.py retrieval [-h] [--topic-nrb TOPIC_NRB]
                        [--nrb-conclusions-per-topic NRB_CONCLUSIONS_PER_TOPIC]
                        [--nrb-premises-per-conclusion NRB_PREMISES_PER_CONCLUSION]
                        [--min-length-factor MIN_LENGTH_FACTOR]
                        [--reranking {maximal-marginal-relevance,structural-distance,argument-rank,word-mover-distance}]
                        [--reuse-unranked REUSE_UNRANKED]
                        [--lambda-conclusions LAMBDA_CONCLUSIONS]
                        [--lambda-premises LAMBDA_PREMISES]
                        [--mu-conclusions MU_CONCLUSIONS]
                        [--mu-premises MU_PREMISES] [--wait-for-es]
                        run_name input_path output_path

positional arguments:
  run_name              The run name that will be included in the last column
                        of the trec file.
  input_path            The file path to the directory containing the input
                        files.
  output_path           The file path to the directory containing the output
                        files.

optional arguments:
  -h, --help            show this help message and exit
  --topic-nrb TOPIC_NRB
                        Restrict the current indexing and/or reranking to a
                        given topic number.
  --nrb-conclusions-per-topic NRB_CONCLUSIONS_PER_TOPIC
                        The number of conclusions that should be retrieved
                        from the index per topic.
  --nrb-premises-per-conclusion NRB_PREMISES_PER_CONCLUSION
                        The number of premises that should be retrieved from
                        the index per conclusion.
  --min-length-factor MIN_LENGTH_FACTOR
  --reranking {maximal-marginal-relevance,structural-distance,argument-rank,word-mover-distance}
  --reuse-unranked REUSE_UNRANKED
  --lambda-conclusions LAMBDA_CONCLUSIONS
  --lambda-premises LAMBDA_PREMISES
  --mu-conclusions MU_CONCLUSIONS
  --mu-premises MU_PREMISES
  --wait-for-es
```

## Development setup

The setup is optimized for MSFT's VSCode.

### Create python env for dev tools

Dev tools will live in their own virtual env. This is better for osx systems.

``` shell
$ python -m venv env
$ source env/bin/activate
$ pip install -r requirements.devtools.txt
$ pre-commit install
```

### Debugging

Debugger is attached to the running containers via port `5678`. Look at `.vscode/launch.json` for
information.

1. Right click on the `./docker/docker-compose.dev.yaml`
2. Wait until container is ready
3. Open `python/src/prototype/app.py`
4. Set a Break Point
5. Open debugger Window
6. Execute Debugger

### Data Set: processed Args.me

csv with the following schema.

`id, conclusion, premises, context, sentences`

- `id` is a unique id of an argument
- `conclusion` is the automatically extracted conclusion of an argument as a string
- `premises` is a string that resembles a json
  - json is contains a list of one json object (total nonsens IMHO)
  - the json object has the following keys:
    - `text`: the text of an argument that contains multiple premises
    - `stance`: the stance of the conclusion
    - `annotation`:
- `context` is a string representing a json object with the following keys:
  - `acquisitionTime`
  - `aspects`
  - `discussionTitle`
  - `mode`
  - `sourceDomain`
  - `sourceId`
  - `sourceText`
  - `sourceTextConclusionStart`
  - `sourceTextConclusionEnd`
  - `sourceTextPremiseStart`
  - `sourceTextPremiseEnd`
  - `sourceTitle`
  - `sourceUrl`
- `sentences` is a string of a list of json objects
  - one json object in the list corresponds to one sentence
  - the last object is the conclusion
  - all objects preceeding are premisis
  - one object has the following keys
    - `sent_id`
    - `sent_text`
